# php-extended/php-certificate-provider-mozilla

A provider of certificates bundle of root authorities from the mozilla releases.

![coverage](https://gitlab.com/php-extended/php-certificate-provider-mozilla/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-certificate-provider-mozilla/badges/master/coverage.svg?style=flat-square)

This library was made to answer a common error :
`SSL certificate problem: unable to get local issuer certificate`
without disabling the certificate when calling the resolving library (like cURL)
and without changing the server configuration in ini files.

This library uses the [https://curl.haxx.se/ca/cacert.pem](https://curl.haxx.se/ca/cacert.pem)
source file to update its contents. This library is updated once a week, every sunday.


## Last Updated Date : 2025-01-26


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-certificate-provider-mozilla ^8`


## Basic Usage

You may use this library the following way:

```php

use PhpExtended\Certificate\MozillaCertificateProvider;

$ch = curl_init();

// set some curl options

$provider = new MozillaCertificateProvider();
$cacert_path = $provider->getCertificateFilePath();

curl_setopt($ch, CURLOPT_CAINFO, $cacert_path);
curl_setopt($ch, CURLOPT_CAPATH, $cacert_path);

curl_exec($ch);

curl_close($ch);

```


## License

MIT (See [license file](LICENSE)).