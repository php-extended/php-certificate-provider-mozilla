<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-certificate-provider-mozilla library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Certificate\MozillaCertificateProvider;
use PHPUnit\Framework\TestCase;

/**
 * MozillaCertificateProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Certificate\MozillaCertificateProvider
 *
 * @internal
 *
 * @small
 */
class MozillaCertificateProviderTest extends TestCase
{
	
	/**
	 * The provider.
	 * 
	 * @var MozillaCertificateProvider
	 */
	protected MozillaCertificateProvider $_provider;
	
	public function testIsFile() : void
	{
		$this->assertTrue(\is_file($this->_provider->getCertificateFilePath()));
	}
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_provider).'@'.\spl_object_hash($this->_provider), $this->_provider->__toString());
	}
	
	protected function setUp() : void
	{
		$this->_provider = new MozillaCertificateProvider();
	}
	
}
