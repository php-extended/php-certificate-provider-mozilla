#!/bin/bash
# This script is to update the data file and push the update
set -eu
IFS=$'\n\t'

CURRDIR=`dirname "$0"`
CURRDIR=`realpath "$CURRDIR"`

echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
wget https://curl.haxx.se/ca/cacert.pem -O "$CURRDIR/dat/cacert.pem"

README=$(cat "$CURRDIR/README.md")
CURDATE=`date +%Y-%m-%d`
SED="s/Last Updated Date : [0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]/Last Updated Date : $CURDATE/g"
README=`echo "$README" | sed "$SED"`
printf "%s" "$README" > "$CURRDIR/README.md"

cd "$CURRDIR"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git pull --all
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
GITTAG=`git describe --abbrev=0 --tags`
GITTAG=`php -r "echo implode('.', [explode('.', '$GITTAG')[0], explode('.', '$GITTAG')[1], ((string) (1 + ((int) explode('.', '$GITTAG')[2])))]);"`
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git add --all
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git commit -m "Automatic update at $CURDATE"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git tag "$GITTAG"
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git push --all ssh://git@gitlab.com/php-extended/php-certificate-provider-mozilla
echo "[$(date '+%Y-%m-%d %H:%M:%S')]"
git push --tags ssh://git@gitlab.com/php-extended/php-certificate-provider-mozilla
echo "END OF SCRIPT -- SUCCESS"
