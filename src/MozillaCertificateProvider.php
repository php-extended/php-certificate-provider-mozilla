<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-certificate-provider-mozilla library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Certificate;

/**
 * MozillaCertificateProvider class file.
 * 
 * This class provides the path to the file that is bundled with this library.
 * 
 * @author Anastaszor
 */
class MozillaCertificateProvider implements CertificateProviderInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Certificate\CertificateProviderInterface::getCertificateFilePath()
	 */
	public function getCertificateFilePath() : string
	{
		return \dirname(__DIR__).\DIRECTORY_SEPARATOR.'dat'.\DIRECTORY_SEPARATOR.'cacert.pem';
	}
	
}
